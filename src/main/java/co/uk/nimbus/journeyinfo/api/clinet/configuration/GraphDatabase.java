package co.uk.nimbus.journeyinfo.api.clinet.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by shivaraj on 10/2/16.
 */
public class GraphDatabase {
    @JsonProperty
    private String schoolBusUrl;
    @JsonProperty
    private  String busUrl;

    @JsonProperty
    private String username;
    @JsonProperty
    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getSchoolBusUrl() {
        return schoolBusUrl;
    }

    public String getBusUrl() {
        return busUrl;
    }
}
