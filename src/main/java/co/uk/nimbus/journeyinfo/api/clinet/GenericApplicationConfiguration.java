package co.uk.nimbus.journeyinfo.api.clinet;

import co.uk.nimbus.journeyinfo.api.clinet.configuration.GraphDatabase;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by rajareddykarri on 25/01/16.
 */
public class GenericApplicationConfiguration extends Configuration {

    @Valid
    @NotNull
    @JsonProperty
    private JerseyClientConfiguration httpClient = new JerseyClientConfiguration();

    public JerseyClientConfiguration getJerseyClientConfiguration() {
        return httpClient;
    }
}
