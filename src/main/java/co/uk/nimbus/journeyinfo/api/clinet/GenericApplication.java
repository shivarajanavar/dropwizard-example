package co.uk.nimbus.journeyinfo.api.clinet;

import co.uk.nimbus.journeyinfo.api.GenericResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import javax.ws.rs.client.Client;


/**
 * Created by rajareddykarri on 25/01/16.
 */
public class GenericApplication extends Application<GenericApplicationConfiguration> {
    public static void main(String[] args) throws Exception {



        if (args.length == 2) {
            new GenericApplication().run(args);
        } else {
            new GenericApplication().run(new String[]{"server", GenericApplication.class.getClassLoader().getResource("api-service.yml").getFile()});
        }
    }

    @Override
    public void initialize(Bootstrap<GenericApplicationConfiguration> bootstrap) {
        super.initialize(bootstrap);
        /*
        * Serve static assets from resources/assets folder
        * Will be accessible as http://localhost:8080/assets/file.txt
        */
        bootstrap.addBundle(new AssetsBundle("/assets/", "/assets/"));
    }

    @Override
    public void run(GenericApplicationConfiguration GenericApplicationConfiguration, Environment environment) throws Exception {
        final Client client = new JerseyClientBuilder(environment).using(GenericApplicationConfiguration.getJerseyClientConfiguration())
                .using(environment)/*.using(basicCredentialsProvider)*/
                .build("HTTP Client");

        JerseyEnvironment jersey = environment.jersey();
        jersey.register(new GenericResource(client));

    }
}
