package co.uk.nimbus.journeyinfo.api;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

/**
 * Created by rajareddykarri on 25/01/16.
 */
@Path("/data")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class GenericResource {

    private Client client;


    public GenericResource(Client client) {
        this.client = client;

    }
    //http://localhost:9090/data?data=hi
    @POST
    public String getData(@QueryParam("data") String data) throws Exception {

        return  data;

    }

  /*  //http://localhost:9090/data?num1=hi&num2=2
    @GET
    public int getSum(@QueryParam("data") int num1, @QueryParam("data") int num2) throws Exception {

      int sum=num1+num2;

        return sum;
    }
*/

}
