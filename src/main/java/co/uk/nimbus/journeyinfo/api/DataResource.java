package co.uk.nimbus.journeyinfo.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by shivaraj on 1/4/16.
 */
public class DataResource {
@JsonProperty("num1")
   int num1;
    @JsonProperty("num2")
    int num2;
    @JsonProperty("sum")
    int sum;

    public int getSum() {

        return sum=num1+num2;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }


}
